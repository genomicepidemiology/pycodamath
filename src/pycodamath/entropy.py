# -*- coding: utf-8 -*-
''' Information-theoretic metrics for pyCoDaMath
'''

__author__ = "Christian Brinch"
__copyright__ = "Copyright 2024"
__credits__ = ["Christian Brinch"]
__license__ = "AFL 3.0"
__version__ = "1.0"
__maintainer__ = "Christian Brinch"
__email__ = "cbri@food.dtu.dk"

import numpy as np
import pandas as pd
from scipy.special import loggamma, polygamma
from tqdm import tqdm


def Derivative(vec, beta):
    k = len(vec)
    s1 = 0
    for n in vec:
        for m in range(n):
            d = m+beta
            s1 += 1./d
    s2 = 0
    for n in range(sum(vec)):
        d = n+k*beta
        s2 += k/d
    q = s1-s2
    return q


def init():
    ''' Initialize entropy extension '''
    @ pd.api.extensions.register_dataframe_accessor("entropy")
    class _:
        ''' An entropy extension to pandas objects containing counts '''

        def __init__(self, pandas_obj):
            self._obj = pandas_obj

        def beta_star(self, niter=10):
            beta = pow(10, np.linspace(-8, 7, 16))
            result = []
            for ns in tqdm([self._obj.iloc[idx] for idx in range(len(self._obj))]):
                for iter in range(niter):
                    if iter == 0:
                        betas = beta
                    b = betas[0]
                    q = Derivative(ns, b)
                    s = np.sign(q)
                    for b in betas:
                        q = Derivative(ns, b)
                        if q*s < 0:
                            # change of sign
                            if iter == 0:
                                betas = [b/10+n*b/10 for n in range(11)]
                                # print('i0 change',b)
                                break
                            else:
                                db = betas[1]-betas[0]
                                betas = [b-db+db/10*n for n in range(11)]
                                # print('change',b,q)
                                break

                # If no solution was found, select among extremes
                if b == beta[-1]:
                    if Derivative(ns, b) < 0:
                        b = beta[0]
                # Done
                result.append(b)
            return result, [Derivative(self._obj.iloc[i], result[i]) for i in range(len(result))]

        def ww(self):
            '''
            Wolpert-Wolf Shannon Entropy estimated from counts ns, given the 
            value of the hyperparameter beta
            '''

            beta = self.beta_star()[0]
            K = len(self._obj.T)
            N = self._obj.T.sum()

            ww_ent = [polygamma(0, N[i]+K*beta[i]+1) - sum((self._obj.iloc[i]+beta[i]) /
                                                           (N[i]+K*beta[i]) * polygamma(0, self._obj.iloc[i]+beta[i]+1)) for i in range(len(self._obj))]
            return [entropy/np.log(K) for entropy in ww_ent]
